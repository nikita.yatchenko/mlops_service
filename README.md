mlops_service
==============================

Service for training a model on a user provided data

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


## Run the app

1. `poetry install`
2. `poetry shell`
3. `uvicorn main:app --reload`

# REST API

The REST API to the example app is described below.

## Get list of models

### Request

`GET /model_list/`

    curl -i -H 'Accept: application/json' http://localhost:8000/model_list/

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    Connection: close
    Content-Type: application/json

    {
    "model_list_status": {
        "random_forest": "OK",
        "support_vector_machines": "Not implemented",
        "light_gradient_boosting": "Not implemented"
        }
    }

## Train a model

### Request

`POST /train/`

    {
    "task_name": "test",
    "cls_task": false,
    "data": {"0": [0.5488135 , 0.71518937, 0.60276338, 0.54488318],
             "1": [0.4236548, 0.64589411, 0.43758721, 0.891773  ],
             "2": [0.96366276, 0.38344152, 0.79172504, 0.52889492],
             "3": [0.56804456, 0.92559664, 0.07103606, 0.0871293 ],
             "4": [0.0202184,  0.83261985, 0.77815675, 0.87001215]},
    "target_column": -1,
    "hyperparameters": {"n_estimators": 40},
    "modeling_type": "random_forest"
    }

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    Connection: close
    Content-Type: application/json

    null

## Make a prediction

### Request

`GET /predict/`

    {
    "task_name": "test_api",
    "data": {"4": [0.5488135 , 0.60276338, 0.54488318],
             "0": [0.4248, 0.649411, 0.891773  ],
             "2": [0.276, 0.4152, 0.504],
             "3": [0.04456, 0.2559664, 0.071236]}
    }

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    Connection: close
    Content-Type: application/json

    {
    "predictions": {
        "0": 0.750840987,
        "1": 0.81209187,
        "2": 0.853309107,
        "3": 0.40681069500000006
    }
    }

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
