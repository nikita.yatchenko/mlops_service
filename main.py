import logging
import os
from pathlib import Path
from time import time
from typing import AnyStr, Dict

import numpy as np
from fastapi import FastAPI, HTTPException, status
from fastapi.responses import PlainTextResponse

from src.data.models import (
    DeleteModelRequest,
    GBParams,
    PredictRequest,
    RandomForestParams,
    TrainRequest,
)
from src.models.gradient_boosting_model import GradientBoosting
from src.models.random_forest_model import RandomForest

app = FastAPI()

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=log_fmt)

PROJECT_PATH = Path(__file__).resolve().parents[2]
PROJECT_PATH = Path(PROJECT_PATH, 'PycharmProjects', 'mlops_service')
MODEL_PATH = Path(PROJECT_PATH, 'models')


@app.get("/", response_class=PlainTextResponse)
def get_starter() -> AnyStr:
    """
    Get request for the homepage
    Returns
    -------
    A string with brief introduction to the service's functionality
    """
    response = {
        'information': '''Hello, this is a service for training model with user provided data \n
                          Available models: \n
                          - RandomForest \n
                          - Gradient Boosted Trees \n
                          - Support Vector Machines \n

                          Please you the following endpoints: \n
                          1. "/model_info" - will let you know of the status of current models \n
                          2. "/train" - will train your selected class of models \n
                          3. "/predict" - will make a prediction with a trained model if exists \n
                          ''',
    }

    return response['information']


@app.get("/model_class_list")
def model_class_list():
    """
    Get request for available models for training
    Returns
    -------
    available models for training
    """
    logger = logging.getLogger(__name__)

    response = {
        'available_classes_to_train': {'random_forest': 'Not implemented',
                                       'support_vector_machines': 'Not implemented',
                                       'gradient_boosting': 'Not implemented'},
    }
    model_files = [i.split('_model')[0] for i in os.listdir(Path(PROJECT_PATH, 'src', 'models'))]
    logger.debug(model_files)
    available_classes_to_train = {k: ('OK' if k in model_files else v) for k, v in
                                  response['available_classes_to_train'].items()}
    response['available_classes_to_train'] = available_classes_to_train

    return response


@app.get("/trained_models")
def trained_models():
    """
    Get request for trained models
    Returns
    -------
    trained models
    """
    logger = logging.getLogger(__name__)

    response = {
        'models_trained_available': {'random_forest': None,
                                     'support_vector_machines': None,
                                     'gradient_boosting': None},
    }
    model_files = [i.split('.joblib')[0] for i in os.listdir(MODEL_PATH) if 'joblib' in i]
    logger.debug(model_files)
    models_trained_available = {k: (True if k in model_files else v) for k, v in
                                response['models_trained_available'].items()}
    response['models_trained_available'] = models_trained_available

    return response


@app.delete("/delete_model", status_code=status.HTTP_200_OK)
def delete_model(model_name: DeleteModelRequest) -> None:
    """
    Delete request for a trained model
    Returns
    -------
    null
    """
    logger = logging.getLogger(__name__)
    model_path = Path(MODEL_PATH, model_name.modeling_type + '.joblib')
    if os.path.exists(str(model_path)):
        logger.info('removing model: {}'.format(str(model_path)))
        os.remove(str(model_path))
    else:
        raise HTTPException(status_code=404, detail="Trained model not found")


@app.post("/train", status_code=status.HTTP_200_OK)
def train(user_data: TrainRequest):
    logger = logging.getLogger(__name__)

    data = user_data.data
    tr_data = np.array(list(data.values()))
    if user_data.target_column != -1:
        x_vals = np.concatenate(tr_data[:, :user_data.target_column],
                                tr_data[:, user_data.target_column:], axis=-1)
    else:
        x_vals = tr_data[:, :user_data.target_column]
    y_vals = tr_data[:, user_data.target_column]
    logger.info('processed user data, shape: {}'.format(x_vals.shape))

    logger.info('initiating a model')
    start = time()
    if user_data.modeling_type == 'random_forest':
        hyperparams = RandomForestParams(**user_data.hyperparameters)
        model = RandomForest(user_data.cls_task, hyperparams)
    elif user_data.modeling_type == 'gradient_boosting':
        hyperparams = GBParams(**user_data.hyperparameters)
        model = GradientBoosting(user_data.cls_task, hyperparams)
    elif user_data.modeling_type == 'support_vector_machines':
        logger.info('Not implemented yet')
        raise HTTPException(status_code=404,
                            detail="{} has not been implemented".format(user_data.modeling_type))
    else:
        raise HTTPException(status_code=501, detail="{} is not supported".format(user_data.modeling_type))
    model.train(x_vals, y_vals)
    end = time()
    logger.info('done with training, time took: {} seconds'.format(str(end - start)))
    model.save_model()


@app.get("/predict", status_code=status.HTTP_200_OK)
def predict(user_data: PredictRequest) -> Dict:
    logger = logging.getLogger(__name__)
    response = {}
    data = user_data.data
    pred_data = np.array(list(data.values()))
    logger.info('processed user data, shape: {}'.format(pred_data.shape))
    if user_data.modeling_type == 'random_forest':
        model = RandomForest(None, None)
    elif user_data.modeling_type == 'gradient_boosting':
        model = GradientBoosting(None, None)
    elif user_data.modeling_type == 'support_vector_machines':
        logger.info('Not implemented yet')
        raise HTTPException(status_code=404,
                            detail="{} has not been trained".format(user_data.modeling_type))
    else:
        raise HTTPException(status_code=404,
                            detail="{} is not supported".format(user_data.modeling_type))
    logger.info('loading: {}'.format(str(MODEL_PATH)))
    try:
        model.load_model(MODEL_PATH)
        logger.info('loaded')
    except Exception as e:
        logger.error(e)
        logger.debug(user_data.modeling_type)
        raise HTTPException(status_code=404,
                            detail="{} has not been trained".format(user_data.modeling_type))
    predictions = model.predict(pred_data)
    response['predictions'] = {i: predictions[i] for i in range(pred_data.shape[0])}

    return response
