from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Service for training a model on a user provided data',
    author='Nikita Yatchenko',
    license='MIT',
)
