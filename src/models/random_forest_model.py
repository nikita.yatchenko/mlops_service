import sys
from pathlib import Path

PROJECT_PATH = Path(__file__).resolve().parents[2]
sys.path.append(str(PROJECT_PATH))
MODEL_PATH = Path(PROJECT_PATH, 'PycharmProjects', 'mlops_service', 'models')

import joblib
import numpy as np
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor

from src.data.models import RandomForestParams
from src.models.abstract_model import Model


class RandomForest(Model):
    def __init__(self, cls_task: bool | None, hyperparams: RandomForestParams | None):
        self.__cls_task = cls_task
        if hyperparams:
            if self.__cls_task:
                self.__model = RandomForestClassifier(**hyperparams.model_dump())
            else:
                self.__model = RandomForestRegressor(**hyperparams.model_dump())
        else:
            self.__model = None
        self.__model_path = None

    @property
    def cls_task(self):
        return self.__cls_task

    @property
    def model(self):
        return self.__model

    def train(self, train_data: np.array, target: np.array) -> None:
        self.__model.fit(train_data, target)

    def predict(self, predict_data: np.array) -> np.array:
        # todo: check if fitted
        return self.__model.predict(predict_data)

    def save_model(self) -> None:
        path_to_save = Path(PROJECT_PATH, 'models')
        path_to_save.mkdir(parents=True, exist_ok=True)
        path_to_save = Path(path_to_save, 'random_forest.joblib')
        self.__model_path = path_to_save

        joblib.dump(self.__model, path_to_save)

    def load_model(self, load_path) -> None:
        load_path = Path(load_path, 'random_forest.joblib')
        self.__model = joblib.load(load_path)

    @property
    def model_path(self):
        return self.__model_path

    @model_path.setter
    def model_path(self, value):
        self.__model_path = value

    @model_path.getter
    def model_path(self):
        return self.__model_path


if __name__ == '__main__':
    np.random.seed(0)
    data = {i: np.random.random(4) for i in range(5)}
    tr_data = np.array(list(data.values()))
    X_vals = tr_data[:, :-1]
    y_vals = tr_data[:, -1]
    hps = RandomForestParams(
        n_estimators=10,
    )
    model = RandomForest(False, hps)
    model.train(X_vals, y_vals)
    model.save_model()
    print(model.predict(X_vals))

    model1 = RandomForest(False, hps)
    model1.load_model(str(MODEL_PATH))
    print(model1.predict(X_vals))
