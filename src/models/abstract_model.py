from abc import ABC, abstractmethod


class Model(ABC):
    @property
    @abstractmethod
    def cls_task(self):
        ...

    @property
    @abstractmethod
    def model(self):
        ...

    @property
    @abstractmethod
    def model_path(self):
        ...

    @model_path.setter
    @abstractmethod
    def model_path(self, value):
        ...

    @abstractmethod
    def train(self, *args, **kwargs):
        ...

    @abstractmethod
    def predict(self, *args, **kwargs):
        ...

    @abstractmethod
    def save_model(self, *args):
        ...

    @abstractmethod
    def load_model(self, *args):
        ...
