from typing import Dict, List, Literal, Union

from pydantic import BaseModel


class TrainRequest(BaseModel):
    modeling_type: Literal["random_forest", "support_vector_machines", "gradient_boosting"] = "random_forest"
    cls_task: bool
    data: Dict[int, List[str | float | int]]
    target_column: int
    hyperparameters: Dict[str, Union[int | str | None]]


class PredictRequest(BaseModel):
    modeling_type: Literal["random_forest", "support_vector_machines", "gradient_boosting"] = "random_forest"
    data: Dict[int, List[str | float | int]]


class DeleteModelRequest(BaseModel):
    modeling_type: Literal["random_forest", "support_vector_machines", "gradient_boosting"] = "random_forest"


class RandomForestParams(BaseModel):
    n_estimators: int
    criterion: Literal["squared_error", "absolute_error", "friedman_mse", "poisson"] = "squared_error"
    max_depth: int = None
    min_samples_split: int = 2
    min_samples_leaf: int or float = 1
    max_features: Literal["sqrt", "log2"] | int | float = 1.0
    random_state: int = 101


class GBParams(BaseModel):
    n_estimators: int = 10
    learning_rate: float = 0.1
    max_depth: int = 3
    max_features: Literal["sqrt", "log2"] | int | float = 1.0
    random_state: int = 101
